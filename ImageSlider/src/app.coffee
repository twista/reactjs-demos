
ImageSlider = React.createClass
    displayName: "ImageSlider"

    getInitialState: ()->
        {total: 0, active: 0}

    componentWillMount: ()->
        @setState {
            total: @props.images.length
        }

    onNavigateClick: (index)->
        @setState {active: index}

    render: ()->
        React.DOM.div {className: "sl-slider"}, [
            React.DOM.img {src: @props.images[@state.active]}
            if @props.pager     then (SliderPager({total: @state.total, active: @state.active, onClick: @onNavigateClick}))
            if @props.navigator then (SliderNavigator({total: @state.total, active: @state.active, onClick: @onNavigateClick}))
        ]

SliderNavigator = React.createClass
    displayName: "Navigator"
    render: ()->
        next = @props.active + 1
        prev = @props.active - 1
        console.log prev, next
        if next >= @props.total then next = 0
        if prev < 0 then prev = (@props.total - 1)
        console.log prev, next

        React.DOM.div {className: "sl-navigator"}, [
            React.DOM.a {onClick: @props.onClick.bind null, prev}, "<< prev"
            " | "
            React.DOM.a {onClick: @props.onClick.bind null, next}, "next >>"
        ]


SliderPager = React.createClass
    displayName: "Pager"

    render: ()->

        pager = []
        for i in [0...@props.total]
            pager.push React.DOM.li {onClick: @props.onClick.bind null, i},
                React.DOM.input {type: "radio", checked: (i == @props.active)}

        React.DOM.ul {className: "sl-pager"}, pager


config = {
    images: [
        "https://s3-us-west-2.amazonaws.com/s.cdpn.io/5689/rock.jpg"
        "https://s3-us-west-2.amazonaws.com/s.cdpn.io/5689/grooves.jpg"
        "https://s3-us-west-2.amazonaws.com/s.cdpn.io/5689/arch.jpg"
        "https://s3-us-west-2.amazonaws.com/s.cdpn.io/5689/sunset.jpg"
    ]
    navigator: true
    pager: true

}

React.renderComponent(ImageSlider(config), document.getElementById("container"))